//
//  PluginInstal.m
//  PluginInstal
//
//  Created by Giuseppe Lapenta on 13/02/15.
//  Copyright (c) 2015 Balzo. All rights reserved.
//

#import "PluginInstal.h"
#import "AdsWrapper.h"

#define OUTPUT_LOG(...)     if (self.debug) NSLog(__VA_ARGS__);

@implementation PluginInstal

@synthesize debug = __debug;
@synthesize strPublishID = __PublishID;
@synthesize interstitialID = __InterstitialID;


- (void) configDeveloperInfo: (NSMutableDictionary*) devInfo
{
    self.strPublishID = (NSString*) [devInfo objectForKey:@"InstalAdID"];
    self.interstitialID = (NSString *) [devInfo objectForKey:@"InstalInterstitialID"];
}

- (void) showAds: (NSMutableDictionary*) info position:(int) pos
{
    if (self.strPublishID == nil ||
        [self.strPublishID length] == 0) {
        OUTPUT_LOG(@"configDeveloperInfo() not correctly invoked in InstalAd!");
        return;
    }
    
    NSString* strType = [info objectForKey:@"INAdType"];
    int type = [strType intValue];
    switch (type) {
        case kTypeBanner:
        {
            NSString* strSize = [info objectForKey:@"INAdSizeEnum"];
            int sizeEnum = [strSize intValue];
            [self showBanner:sizeEnum atPos:pos];
            break;
        }
        case kTypeInterstitial:
            [self loadInterstial];
            break;
        default:
            OUTPUT_LOG(@"The value of 'INAdType' is wrong (should be 1 or 2)");
            break;
    }
}

- (void) hideAds: (NSMutableDictionary*) info
{
    NSString* strType = [info objectForKey:@"INAdType"];
    int type = [strType intValue];
    switch (type)
    {
        case kTypeBanner:
        {
            if (nil != self.bannerView) {
                [self.bannerView removeFromSuperview];
                self.bannerView = nil;
            }
            break;
        }
        case kTypeInterstitial:
            OUTPUT_LOG(@"Now not support full screen view in FBAd");
            break;
        default:
            OUTPUT_LOG(@"The value of 'INAdType' is wrong (should be 1 or 2)");
            break;
    }
}

- (void) queryPoints
{
    OUTPUT_LOG(@"INAd not support query points!");
}

- (void) spendPoints: (int) points
{
    OUTPUT_LOG(@"INAd not support spend points!");
}

- (void) setDebugMode: (BOOL) isDebugMode
{
    self.debug = isDebugMode;
}

- (NSString*) getSDKVersion
{
    return @"3.17.1";
}

- (NSString*) getPluginVersion
{
    return @"1.0";
}

- (void) showBanner:(int)sizeEnum atPos:(int)pos
{
//    FBAdSize size = kFBAdSize320x50;
//    switch (sizeEnum)
//    {
//        case kSizeDefault:
//            size = kFBAdSize320x50;
//            break;
//            
//        case kSizeInterstital:
//        case kSizeHeightFixed:
//        {
//            BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
//            size = (isIPAD) ? kFBAdSizeHeight90Banner : kFBAdSizeHeight50Banner;
//        }
//            break;
//        default:
//            break;
//    }
    
    if (nil != self.bannerView) {
        self.bannerView.delegate = nil;
        [self.bannerView removeFromSuperview];
        self.bannerView = nil;
    }
    
    
    // Create a banner's ad view with a unique placement ID (generate your own on the Facebook app settings).
    // Use different ID for each ad placement in your app.
    self.bannerView = [[INAdView alloc] initWithAdUnitId:self.strPublishID];
    
    // Set a delegate to get notified on changes or when the user interact with the ad.
    self.bannerView.delegate = self;
    self.bannerView.frame = CGRectMake(0, 0, 320, 50);
    
    
    // Initiate a request to load an ad.
    [self.bannerView loadAd];
    
    [AdsWrapper addAdView:self.bannerView atPos:pos];
}

- (void) loadInterstial
{
    // Create the interstitial unit with a placement ID (generate your own on the Facebook app settings).
    // Use different ID for each ad placement in your app.
    self.interstitialView = [INInterstitialAdController interstitialAdControllerForAdUnitId:self.interstitialID];
    
    // Set a delegate to get notified on changes or when the user interact with the ad.
    self.interstitialView.delegate = self;
    
    // Initiate the request to load the ad.
    [self.interstitialView loadAd];
}

- (void) showInterstitial
{
    if (!self.interstitialView)
    {
        // Ad not ready to present.
        OUTPUT_LOG(@"Ad not loaded.");
        
    } else {
        
        // Ad is ready, present it!
        [self.interstitialView showFromViewController:[AdsWrapper getCurrentRootViewController]];
        [AdsWrapper onAdsResult:self withRet:kAdsShown withMsg:@"Ads is shown!"];
        
    }
    
}

#pragma mark - INInterstitialAdControllerDelegate

- (void)interstitialDidLoadAd:(INInterstitialAdController *)interstitial {
    
    [AdsWrapper onAdsResult:self withRet:kAdsReceived withMsg:@"Interstitial Ad request received success!"];
    [self showInterstitial];
}

- (void)interstitialDidFailToLoadAd:(INInterstitialAdController *)interstitial{
    
    [AdsWrapper onAdsResult:self withRet:kUnknownError withMsg:@"fail load interstial"];

}

#pragma mark - INAdViewDelegate


- (UIViewController *)viewControllerForPresentingModalView {
    return [AdsWrapper getCurrentRootViewController];
}

- (void)adViewDidLoadAd:(INAdView *)view {
    [AdsWrapper onAdsResult:self withRet:kAdsReceived withMsg:@"Banner loaded"];
}

- (void)adViewDidFailToLoadAd:(INAdView *)view {
    [AdsWrapper onAdsResult:self withRet:kUnknownError withMsg:@"Failed loading banner"];
}





@end
