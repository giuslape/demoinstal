//
//  PluginInstal.h
//  PluginInstal
//
//  Created by Giuseppe Lapenta on 13/02/15.
//  Copyright (c) 2015 Balzo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InterfaceAds.h"
#import "INAdView.h"
#import "INInterstitialAdController.h"

typedef enum {
    
    kSizeDefault = 1,
    kSizeHeightFixed,
    kSizeInterstital,
} FBAdSizeEnum;

typedef enum {
    kTypeBanner = 1,
    kTypeInterstitial,
} FBAdType;

@interface PluginInstal : NSObject <InterfaceAds, INAdViewDelegate, INInterstitialAdControllerDelegate>

@property BOOL debug;
@property (copy, nonatomic) NSString* strPublishID;
@property (copy, nonatomic) NSString* interstitialID;
@property (nonatomic, strong, readwrite) INAdView *bannerView;
@property (nonatomic, strong, readwrite) INInterstitialAdController *interstitialView;

/**
 interfaces from InterfaceAds
 */
- (void) configDeveloperInfo: (NSMutableDictionary*) devInfo;
- (void) showAds: (NSMutableDictionary*) info position:(int) pos;
- (void) hideAds: (NSMutableDictionary*) info;
- (void) queryPoints;
- (void) spendPoints: (int) points;
- (void) setDebugMode: (BOOL) isDebugMode;
- (NSString*) getSDKVersion;
- (NSString*) getPluginVersion;


@end
