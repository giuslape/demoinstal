#include "HelloWorldScene.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "PluginManager.h"
#include "ProtocolAds.h"

using namespace cocos2d::plugin;

extern ProtocolAds* _instalAd;
#endif

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Demo Instal", "fonts/Marker Felt.ttf", 32);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(label, 1);

    
    //Menu for handle banner
    Menu* pMenu = Menu::create();
    pMenu->setPosition( Point::ZERO );
    
    Point posMid = Point(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2);
    Point posBR = Point(origin.x + visibleSize.width, origin.y);

    
    Label* label1 = Label::createWithSystemFont("ShowAds", "Arial", 32);
    MenuItemLabel* pItemShow = MenuItemLabel::create(label1, CC_CALLBACK_1(HelloWorld::showBanner, this));
    pItemShow->setAnchorPoint(Point(0.5f, 0));
    pMenu->addChild(pItemShow, 0);
    pItemShow->setPosition(posMid + Point(-100, -120));
    
    Label* label2 = Label::createWithSystemFont("HideAds", "Arial", 32);
    MenuItemLabel* pItemHide = MenuItemLabel::create(label2, CC_CALLBACK_1(HelloWorld::hideBanner, this));
    pItemHide->setAnchorPoint(Point(0.5f, 0));
    pMenu->addChild(pItemHide, 0);
    pItemHide->setPosition(posMid + Point(100, -120));
    
    Label* label3 = Label::createWithSystemFont("Show Interstitial", "Arial", 32);
    MenuItemLabel* pItemInterstitial = MenuItemLabel::create(label3, CC_CALLBACK_1(HelloWorld::showInterstitial, this));
    pItemInterstitial->setAnchorPoint(Point(0.5f, 0));
    pMenu->addChild(pItemInterstitial, 0);
    pItemInterstitial->setPosition(posMid);

    
    this->addChild(pMenu);
    
    return true;
}

void HelloWorld::showBanner(Ref *sender)
{
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

    cocos2d::plugin::TAdsInfo adInfo;
    adInfo["INAdType"] = "1";
    adInfo["INAdSizeEnum"] = "2";
    
    cocos2d::plugin::ProtocolAds::AdsPos _pos;
    _pos = ProtocolAds::kPosBottom;
    _instalAd->showAds(adInfo,_pos);
#endif
}

void HelloWorld::hideBanner(cocos2d::Ref *sender)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    cocos2d::plugin::TAdsInfo adInfo;
    adInfo["INAdType"] = "1";
    _instalAd->hideAds(adInfo);
#endif
}

void HelloWorld::showInterstitial(Ref *sender)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    cocos2d::plugin::TAdsInfo adInfo;
    adInfo["INAdType"] = "2";
    adInfo["INAdSizeEnum"] = "2";
    
    cocos2d::plugin::ProtocolAds::AdsPos _pos;
    _pos = ProtocolAds::kPosBottom;
    _instalAd->showAds(adInfo,_pos);
#endif
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
